//
//  AllNotesController.swift
//  KeepNotes
//
//  Created by Masoud Heydari on 7/16/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class AllNotesController: UIViewController {
    
    var notes = [Note]()
    
    let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .white
        return tableView
    }()
    
    let btnFAB: UIButton = {
        let btn = UIButton(type: .custom)
        let plusImg = #imageLiteral(resourceName: "plus.png").withRenderingMode(.alwaysOriginal)
        btn.setImage(plusImg, for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = Color.yellow
        btn.addTarget(self, action: #selector(btnFABTapped(sender:)), for: .touchUpInside)
        return btn
    }()
    
    let noNoteLabel: UIPaddingLabel = {
        let label = UIPaddingLabel(padding: .init(top: 4, left: 4, bottom: 4, right: 4))
        label.text = Const.Controller.AllNotes.noNoteToShow
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .lightGray
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handleShowingNoNoteLabel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barStyle = .black
        
        setupView()
        setupTableView()
        setupNavBar()
        setupSearchBar()
        navigationItem.title = Const.NavTitle.allNotes
        
        // fetch all notes
        self.fetchAllNotes()
    }
    
    private func setupView() {
        view.backgroundColor = .white
        addViews()
        addConstraints()
    }
    
    private func addViews() {
        view.addSubview(tableView)
        view.addSubview(btnFAB)
    }
    
    private func addConstraints() {
        let safeArea = view.safeAreaLayoutGuide
        
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        btnFAB.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -12).isActive = true
        btnFAB.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: -12).isActive = true
        btnFAB.heightAnchor.constraint(equalToConstant: Const.Controller.AllNotes.fabSize).isActive = true
        btnFAB.widthAnchor.constraint(equalToConstant: Const.Controller.AllNotes.fabSize).isActive = true
        
        //set the corner radius
        btnFAB.layer.cornerRadius = Const.Controller.AllNotes.fabSize / 2
        btnFAB.layer.masksToBounds = false
        
        // set the shadow properties
        btnFAB.layer.shadowColor = UIColor.black.cgColor
        btnFAB.layer.shadowOffset = CGSize(width: -1, height: 1.0)
        btnFAB.layer.shadowOpacity = 0.5
        btnFAB.layer.shadowRadius = 4.0
        
        
        print("tb frame : \(tableView.frame)")
    }
    
    private func setupNoNoteLabel() {
        view.addSubview(noNoteLabel)
        
        noNoteLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        noNoteLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    private func handleShowingNoNoteLabel() {
        self.notes.isEmpty ? setupNoNoteLabel() : removeNoNoteLable()
    }
    
    private func removeNoNoteLable() {
        noNoteLabel.removeFromSuperview()
    }
    
    private func setupSearchBar() {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false
        let searchBar = searchController.searchBar
        searchBar.tintColor = UIColor.white
        searchBar.barTintColor = UIColor.white
        searchBar.delegate = self
        
        if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
            textfield.textColor = .blue
            
            textfield.backgroundColor = UIColor(red: 246/255, green: 195/255, blue: 50/255, alpha: 1)
            
            textfield.layer.cornerRadius = 10;
            textfield.clipsToBounds = true;
            
        }
        
        navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationItem.searchController = searchController
    }
    
    @objc private func btnFABTapped(sender: UIButton) {
        print("btn fab tapped!")
        sender.tapped()
        let newNoteController = NewOrEditController(flag: .newNote)
        newNoteController.delegate = self
        let navNewNoteController = UINavigationController(rootViewController: newNoteController)
        present(navNewNoteController, animated: true)
    }
}

/**********************************************/
/***************** CORE DATA  *****************/
/**********************************************/
extension AllNotesController {
    private func fetchAllNotes() {
        CoreData.shared.fetchAll(entityName: Const.DB.noteEntity) { (notes, error) in
            if let error = error {
                // there is some error
                // show some alert
                print("Failed to get all notes, Error -> \(error)")
                return
            }
            
            guard let unwrappedNotes = notes else { return }
            self.notes = unwrappedNotes
            self.tableView.reloadData()
        }
    }
}

/**************************************************/
/***************** UI TABLE VIEW  *****************/
/**************************************************/
extension AllNotesController: UITableViewDelegate, UITableViewDataSource {
    private func setupTableView() {
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(SingleNoteCell.self, forCellReuseIdentifier: Const.CellId.singleNoteCellID)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Const.CellId.singleNoteCellID, for: indexPath) as? SingleNoteCell else { return UITableViewCell() }
        cell.note = self.notes[indexPath.row]
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let editNoteController = NewOrEditController(flag: .editNote)
        editNoteController.note = self.notes[indexPath.row]
        editNoteController.delegate = self
        let navEditNoteController = UINavigationController(rootViewController: editNoteController)
        present(navEditNoteController, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { [weak self] (_, indexPath) in
            guard let strongSelf = self else { return }
            let note = strongSelf.notes[indexPath.row]
            
            strongSelf.notes.remove(at: indexPath.row)
            strongSelf.tableView.deleteRows(at: [indexPath], with: .automatic)
            CoreData.shared.delete(note: note) {
                strongSelf.handleShowingNoNoteLabel()
            }
        }
        
        deleteAction.backgroundColor = UIColor.red
        
        return [deleteAction]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
}

/**********************************************/
/***************** CORE DATA  *****************/
/**********************************************/
extension AllNotesController: NewOrEditeNoteDelegate {
    
    func noteDidCreated(note: Note) {
        notes.append(note)
        let newIndexPath = IndexPath(row: notes.count - 1, section: 0)
        tableView.insertRows(at: [newIndexPath], with: .automatic)
        handleShowingNoNoteLabel()
    }
    
    func noteDidEdited(note: Note) {
        guard let editedRow = notes.firstIndex(of: note) else { return }
        let reloadIndexPath = IndexPath(row: editedRow, section: 0)
        tableView.reloadRows(at: [reloadIndexPath], with: .automatic)
        handleShowingNoNoteLabel()
    }
}

/***************************************************/
/*************** SEARCH BAR DELEGATE ***************/
/***************************************************/

extension AllNotesController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        fetchAllNotes()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("search button clicked!")
        guard let searchText = searchBar.text else { return }
        CoreData.shared.searchInNotes(for: searchText, entityName: Const.DB.noteEntity) { (notes) in
            guard let notes = notes else { return }
            self.notes = notes
            self.tableView.reloadData()
        }
    }
}



extension UIButton {
    public func tapped() {
        UIView.animate(withDuration: 0.2, animations: {
            self.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
        }) { (_) in
            UIView.animate(withDuration: 0.2, animations: {
                self.transform = .identity
            })
        }
    }
}
