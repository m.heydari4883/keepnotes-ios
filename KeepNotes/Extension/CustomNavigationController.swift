//
//  Cu.swift
//  KeepNotes
//
//  Created by Masoud Heydari on 7/18/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

extension UIViewController {
    public func setupNavBar() {
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)

        edgesForExtendedLayout = []
        self.navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = Color.yellow
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor : UIColor.white
        ]
    }
}
