//
//  Color.swift
//  KeepNotes
//
//  Created by Masoud Heydari on 7/18/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

struct Color {
    static let yellow = UIColor(red: 244/255, green: 180/255, blue: 0, alpha: 1)
}
