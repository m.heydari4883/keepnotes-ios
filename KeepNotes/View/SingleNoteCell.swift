//
//  SingleNoteCell.swift
//  KeepNotes
//
//  Created by Masoud Heydari on 7/17/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class SingleNoteCell: UITableViewCell {
    
    private var isReLayouting = false
    
    var note: Note? {
        didSet {
            guard let note = note else { return }
            self.titleTextView.text = note.note_title
            self.noteTextView.text = note.note_desc
        }
    }
    
    let contentContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let titleTextView: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.boldSystemFont(ofSize: 18)
        tv.text = "Title"
        tv.isSelectable = false
        tv.isScrollEnabled = false
        tv.backgroundColor = .white
        tv.textAlignment = .left
        return tv
    }()
    
    let noteTextView: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 14)
        tv.text = "Note"
        tv.backgroundColor = .white
        tv.textColor = .lightGray
        tv.isSelectable = false
        tv.isScrollEnabled = false
        tv.textAlignment = .left
        return tv
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        addViews()
        addConstranits()
    }
    
    private func addViews() {
        let stackView = UIStackView(arrangedSubviews: [
            titleTextView, noteTextView])
        stackView.axis = .vertical
        
        selectionStyle = .none
        
        addSubview(stackView)
        addSubview(contentContainer)
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.leftAnchor.constraint(equalTo: contentContainer.leftAnchor, constant: 8).isActive = true
        stackView.rightAnchor.constraint(equalTo: contentContainer.rightAnchor, constant: -8).isActive = true
        stackView.topAnchor.constraint(equalTo: contentContainer.topAnchor, constant: 4).isActive = true
        stackView.bottomAnchor.constraint(equalTo: contentContainer.bottomAnchor, constant: -4).isActive = true
        
        contentContainer.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        contentContainer.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        contentContainer.topAnchor.constraint(equalTo: topAnchor, constant: 6).isActive = true
        contentContainer.heightAnchor.constraint(lessThanOrEqualToConstant: cSize.noteTableViewCellHeight).isActive = true
        contentContainer.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -6).isActive = true
        
        contentContainer.layer.cornerRadius = 8
        contentContainer.layer.borderColor = UIColor.lightGray.cgColor
        contentContainer.layer.borderWidth = 0.75
    }
    
    private func addConstranits() {

    }

}
